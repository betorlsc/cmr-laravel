@extends('layouts.admin')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Nuevo Usuario</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="POST" action="{{route('admin.users.store')}}">
                                @csrf
                                <div class="form-group">
                                    <label for="name" class="required">Nombre</label>
                                    <input type="text" name="name" id="name" class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}" placeholder="Ingrese el nombre del usuario" value="{{old('name', '')}}">
                                    @if ($errors->has('name'))
                                    <span class="text-danger">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" name="email" id="email"
                                        class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                                        placeholder="Ingrese el email del usuario"
                                        value="{{ old('email', '') }}">
                                    @if ($errors->has('email'))
                                        <span class="text-danger">
                                            <strong> {{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <label for="password">Contraseña</label>
                                    <input type="password" name="password" id="password"
                                        class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}">
                                      
                                    @if ($errors->has('password'))
                                        <span class="text-danger">
                                            <strong> {{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <label for="password-confirmation">Repetir contraseña</label>
                                    <input type="password" name="password_confirmation" id="password_confirmation"
                                        class="form-control "
                                        placeholder="Repita contraseña">

                                </div>

                                <div class="row d-print-none-mt-2">
                                    <div class="col-12 text-right">
                                        <a href="{{ route('admin.users.index')}}" class="btn btn-danger"> 
                                            <i class="fa fa-fw lg fa-arrow-left"></i>
                                            Regresar
                                        </a>
                                        <button type="submit" class="btn btn-success">
                                            <i class="fa fa-fw lg fa-check-circle"></i>
                                            Guardar
                                        </button>
                                    </div>
                                </div>

                            </form>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
