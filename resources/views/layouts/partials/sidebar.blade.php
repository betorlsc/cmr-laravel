<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <span class="brand-text font-weight-light">AdminLTE CRM</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
   with font-awesome or any other icon font library -->
                <li class="nav-item menu-open">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="{{ route('admin.users.index') }}" 
                            class="nav-link {{ \Request::route()->getName() == 'admin.users.index' || \Request::route()->getName() == 'admin.users.edit' || \Request::route()->getName() == 'admin.users.create' ? 'active' : '' }}">
                                <i class="nav-icon fas fa-users"></i>
                                <p>Usuarios</p>
                            </a>
                        </li>
                        <li class="nav-item">

                            <a href="{{ route('admin.clients.index') }}"
                                class="nav-link {{ \Request::route()->getName() == 'admin.clients.index' || \Request::route()->getName() == 'admin.clients.edit' || \Request::route()->getName() == 'admin.clients.create' ? 'active' : '' }}">
                                <i class="nav-icon fas fa-user-tie"></i>
                                <p>Clientes </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin.projects.index') }}" 
                            class="nav-link {{ \Request::route()->getName() == 'admin.projects.index' || \Request::route()->getName() == 'admin.projects.edit' || \Request::route()->getName() == 'admin.projects.create' ? 'active' : '' }}">
                                <i class="nav-icon fas fa-project-diagram"></i>
                                <p>Proyectos</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="{{ route('admin.tasks.index') }}" 
                            class="nav-link {{ \Request::route()->getName() == 'admin.tasks.index' || \Request::route()->getName() == 'admin.tasks.edit' || \Request::route()->getName() == 'admin.tasks.create' ? 'active' : '' }}">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                <p>Tareas </p>
                            </a>
                        </li>
                    </ul>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                        <i class="fas fa-fw fa-sign-out-alt mavi-icon"></i>
                        <p>Cerras sesión </p>
                    </a>


                </li>

                </li>

            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>

    <!-- /.sidebar -->
</aside>
